from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields.related import ForeignKey
from tambahupdate.models import Resep
from django.contrib.auth.models import User
# Create your models here.


class Comment(models.Model):
    resep = models.ForeignKey(Resep, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    komentar = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.komentar
