from django import setup
from django.apps import apps
from django.conf.urls import url
from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User


from .apps import DetailcommentConfig
from .models import Comment
from tambahupdate.models import Resep
from kategori.models import Kategori
from .views import detail
from . import models
import datetime


# Create your tests here.


class TestApp(TestCase):
    def setUp(self):
        user = User.objects.create_user(
            'dummyuser',
            'fortestingpurpose',
            'fortestingpurpose'
        )
        user.save()
        self.client.login(username='dummyuser', password='fortestingpurpose')
        
        aKategori = Kategori.objects.create(nama="Daging")
        resep = Resep.objects.create(
            user = get_user_model().objects.get(username='dummyuser'),
            nama="Rendang",
            kategori=aKategori,
            durasi="2 jam",
            porsi="10 porsi",
            deskripsi="Maknyos",
            bahan="- Daging sapi\n- Bumbu rendang",
            langkah="- Masak Daging sapinya sampai matang\n- Sajikan",
        )

    def test_is_url_exist(self):
        namaResep = Resep.objects.all()[0].nama
        aResponse = Client().get(f'/detail/{ namaResep }/')
        self.assertEquals(namaResep, 'Rendang')
        self.assertEquals(aResponse.status_code, 200)

    def test_is_app_available(self):
        self.assertEqual(DetailcommentConfig.name, 'detailComment')
        self.assertEqual(apps.get_app_config(
            'detailComment').name, 'detailComment')

    def test_is_comment_model_created(self):
        aKategori = Kategori.objects.create(nama="Lain-lain")
        newResep = Resep.objects.create(
            user = get_user_model().objects.get(username='dummyuser'),
            nama='Indomie',
            kategori=aKategori,
            durasi='5 menit',
            porsi='2 porsi',
            deskripsi='Seleraku',
            bahan='1 bungkus indomie dan telur',
            langkah='- Buka bungkusnya\n- masukkan mienya\n- sajikan',
        )
        komeng = Comment.objects.create(resep=newResep, komentar="komen mulu kayak komeng", user=get_user_model().objects.get(username='dummyuser'))
        self.assertTrue(isinstance(komeng, Comment))
        self.assertEqual(len(Comment.objects.all()), 1)
        self.assertEqual(str(komeng), "komen mulu kayak komeng")

    def test_is_detail_page_exists(self):
        response = self.client.post('/detail/Rendang/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "detail.html")
        found = resolve(f'/detail/{Resep.objects.all()[0].nama}/')
        self.assertEqual(found.func, detail)
