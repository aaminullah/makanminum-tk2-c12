from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import ResepForm
from .models import Resep
from kategori.models import Kategori
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import urllib.request, json

@login_required(login_url='/login/')
def tulis(request):
    if request.method == "POST":
        kategori = Kategori.objects.get(id=request.POST['kategori'])
        submit_resep = Resep(
            user = request.user,
            nama = request.POST['nama'],
            kategori = kategori,
            durasi = request.POST['durasi'],
            porsi = request.POST['porsi'],
            deskripsi = request.POST['deskripsi'],
            bahan = request.POST['bahan'],
            langkah = request.POST['langkah'],
            foto = request.FILES.get('foto', None)
        )
        submit_resep.save()
        response_data = {}
        response_data['id_resep'] = submit_resep.id
        response_data['nama'] = submit_resep.nama
        response_data['kategori'] = submit_resep.kategori.nama
        response_data['porsi'] = submit_resep.porsi
        response_data['deskripsi'] = submit_resep.deskripsi
        response_data['bahan'] = submit_resep.bahan
        response_data['langkah'] = submit_resep.langkah
        if (submit_resep.foto):
            response_data['foto'] = submit_resep.foto.url
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    form = ResepForm()
    context = {
        'form' : form,
        'h1' : 'Tulis Resep Baru',
        'p' : 'Tunjukkan kreativitas Anda dengan membagikan resep karya Anda sendiri.',
    }
    return render(request, "resep_form.html", context)

@login_required(login_url='/login/')
def perbarui(request, pk):
    resep = Resep.objects.get(id=pk)
    nama = resep.nama   
    if request.method == "POST":
        form = ResepForm(request.POST or None, request.FILES or None, instance=resep)
        if form.is_valid():
            messages.success(request, f'Resep "{nama}" berhasil diperbarui!')
            form.save()
            return redirect(f'/detail/{resep.nama}/')
    form = ResepForm(instance=resep)
    context = {
        'form' : form,
        'h1' : 'Perbarui Resep',
        'p' : 'Sempurnakan resep yang sudah ada dengan memperbarui informasinya',
    }
    return render(request, "resep_form.html", context)
    
def data_resep_api(request):
    reseps = Resep.objects.all()
    res = []
    for i in reseps:
        temp = dict()
        temp['nama'] = i.nama
        res.append(temp)
    data = {
        'nama' : res,
    }
    return JsonResponse(data)