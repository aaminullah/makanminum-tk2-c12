from django.apps import apps
from django.test import TestCase, Client
from .apps import TambahupdateConfig
from .models import Resep
from kategori.models import Kategori
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.test import Client
from django.urls import resolve
from .views import data_resep_api
from django.core.files.uploadedfile import SimpleUploadedFile

# Create your tests here.
class TestApp(TestCase):
    def setUp(self):
        user = User.objects.create_user(
            'dummyuser',
            'fortestingpurpose',
            'fortestingpurpose'
        )
        user.save()
        self.client.login(username='dummyuser', password='fortestingpurpose')

        Kategori.objects.create(nama="Daging")
        dummy = Kategori.objects.get(nama='Daging')
        Resep.objects.create(
            user = get_user_model().objects.get(username='dummyuser'),
            nama = "Rendang",
            kategori = dummy,
            durasi = "2 jam",
            porsi = "10 porsi",
            deskripsi = "Maknyos",
            bahan = "- Daging sapi\n- Bumbu rendang",
            langkah = "- Masak Daging sapinya sampai matang\n- Sajikan"
        )

    def test_is_app_exist(self):
        self.assertEqual(TambahupdateConfig.name, 'tambahupdate')
        self.assertEqual(apps.get_app_config('tambahupdate').name, 'tambahupdate')
    
    def test_is_resep_model_created(self):
        count = Resep.objects.all().count()
        resep = Resep.objects.create(
            user = get_user_model().objects.get(username='dummyuser'),
            nama = "Indomie",
            kategori = Kategori.objects.create(nama='Lain - Lain'),
            durasi = "10 menit",
            porsi = "1 porsi",
            deskripsi = "Seleraku",
            bahan = "1 bungkus indomie",
            langkah = "- Buka bungkus indomienya\n- Rebus mienya sampai matang\n- Sajikan",
        )
        self.assertTrue(isinstance(resep, Resep))
        self.assertEqual(Resep.objects.all().count(), count + 1)

    def test_tulisresep_url_is_exist(self):
        response = self.client.get('/resep_form/tulis/')
        self.assertEqual(response.status_code, 200)

    def test_does_create_form_from_page_succeed(self):
        count = Resep.objects.all().count()
        Kategori.objects.create(nama='Lain - Lain')
        dummy = Kategori.objects.get(nama='Lain - Lain')
        foto = SimpleUploadedFile(name='NoPhoto.jpg',
            content=open('tambahupdate/static/NoPhoto.jpg', 'rb').read(),
            content_type='image/jpeg'
        )

        data = {
            'nama' : 'Indomie',
            'kategori' : dummy.id,
            'durasi' : '5 menit',
            'porsi' : '2 porsi',
            'deskripsi' : 'Seleraku',
            'bahan' : '1 bungkus indomie dan telur',
            'langkah' : '- Buka bungkusnya\n- masukkan mienya\n- sajikan',
            'foto' : foto
        }
        response = self.client.post('/resep_form/tulis/', data)
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Resep.objects.all().count(), count + 1)
    
    def test_perbaruiresep_url_is_exist(self):
        response = self.client.get(f'/resep_form/perbarui/{Resep.objects.all()[0].id}/')
        self.assertEqual(response.status_code, 200)
    
    def test_does_update_form_from_page_succeed(self):
        resep = Resep.objects.get(nama="Rendang")
        data = {
            'nama' : 'Mi goreng',
            'kategori' : resep.kategori.id,
            'durasi' : '1 jam',
            'porsi' : 'a',
            'deskripsi' : 'a',
            'bahan' : 'a',
            'langkah' : 'a',
        }
        response = self.client.post(f'/resep_form/perbarui/{Resep.objects.all()[0].id}/', data)
        amount = Resep.objects.filter(nama='Rendang').count()
        self.assertEqual(amount, 0)
        self.assertEqual(response.status_code, 302)

    def test_access_tulis_resep_page_while_not_logged_in(self):
        self.client.logout()
        response = self.client.get('/resep_form/tulis/')
        self.assertEqual(response.status_code, 302)
    
    def test_does_data_resep_api_works(self):
        response = self.client.get('/resep_form/data-resep-api/')
        self.assertEqual(response.status_code, 200)
        res = resolve('/resep_form/data-resep-api/')
        self.assertEqual(res.func, data_resep_api)