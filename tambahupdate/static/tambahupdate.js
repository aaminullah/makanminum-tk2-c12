$(document).ready(function(){
    var cek_nama = $("#id_nama").val();
    if (cek_nama == '') {
        $('#id_kategori').prop('disabled', true);
        $('#id_durasi').prop('disabled', true);
        $('#id_porsi').prop('disabled', true);
        $('#id_deskripsi').prop('disabled', true);
        $('#id_bahan').prop('disabled', true);
        $('#id_langkah').prop('disabled', true);
        $('#id_foto').prop('disabled', true);

        $('#submit_button').prop('disabled', true);
        $('#submit_button').css("background-color", "#a16ae8");

        $("#info").append(
            '<div class="alert alert-info alert-dismissible">' + 
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + 
            'Mohon menambahkan resep dengan nama yang unik' + 
            '</div>'
        )
    }
    document.getElementById("id_nama").onkeydown = function(e){
        if(e.keyCode == 13) {
            e.preventDefault();
            $("#cek_button").click();
        }
    }
    if (cek_nama == '') {
        $("#form-resep").submit(function(e){
            e.preventDefault();
            var temp = $("#id_deksripsi").val();
            console.log(temp);
            var formData  = new FormData();
            var nama_resep = $("#id_nama").val();
            formData.append('nama', $("#id_nama").val());
            formData.append('kategori', $("#id_kategori").val());
            formData.append('durasi', $("#id_durasi").val());
            formData.append('porsi', $("#id_porsi").val());
            formData.append('deskripsi', $("#id_deskripsi").val());
            formData.append('bahan', $("#id_bahan").val());
            formData.append('langkah', $("#id_langkah").val());
            if ($("#id_foto")[0].files[0]) {
                formData.append('foto', $("#id_foto")[0].files[0]);
            }
            formData.append('csrfmiddlewaretoken', $('input[name=csrfmiddlewaretoken]').val());
            $.ajax({
                type : "POST",
                url : "/resep_form/tulis/",
                data : formData,
                contentType : false,
                processData : false,
                success : function(){
                    $("#info").empty();
                    $("#cek_result").empty();
                    var result = $("#notif");
                    result.empty();
                    result.append(
                        '<div class="alert alert-success alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                        'Resep dengan nama <strong>' + nama_resep +' </strong>berhasil ditambahkan!' +
                        '</div>'
                    )
                    
                    $("#id_nama").val('');
                    $("#id_kategori").val('');
                    $("#id_durasi").val('');
                    $("#id_porsi").val('');
                    $("#id_deskripsi").val('');
                    $("#id_bahan").val('');
                    $("#id_langkah").val('');
                    $("#id_foto").val('');
                }
            });
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        });
    }

    $("#cek_button").click(function() {
        var input_nama = $("#id_nama").val();
        $.ajax({
            url : "/resep_form/data-resep-api",
            success : function(hasil) {
                console.log(hasil);
                var result = $("#cek_result");
                result.empty();
                if (input_nama == '') {
                    $('#id_kategori').prop('disabled', true);
                    $('#id_durasi').prop('disabled', true);
                    $('#id_porsi').prop('disabled', true);
                    $('#id_deskripsi').prop('disabled', true);
                    $('#id_bahan').prop('disabled', true);
                    $('#id_langkah').prop('disabled', true);
                    $('#id_foto').prop('disabled', true);

                    $('#submit_button').prop('disabled', true);
                    $('#submit_button').css("background-color", "#a16ae8");
                    result.append(
                        '<div class="alert alert-danger alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                        '<strong>Nama resep tidak boleh kosong!</strong>' +
                        '</div>'
                    );
                } else {
                    var flag = false;
                    for (var i = 0; i < hasil.nama.length; i++) {
                        if (hasil.nama[i].nama == input_nama && input_nama != cek_nama) {
                            flag = true;
                        }
                    }
                    if (flag) {
                        $('#id_kategori').prop('disabled', true);
                        $('#id_durasi').prop('disabled', true);
                        $('#id_porsi').prop('disabled', true);
                        $('#id_deskripsi').prop('disabled', true);
                        $('#id_bahan').prop('disabled', true);
                        $('#id_langkah').prop('disabled', true);
                        $('#id_foto').prop('disabled', true);

                        $('#submit_button').prop('disabled', true);
                        $('#submit_button').css("background-color", "#a16ae8");
                        result.append(
                            '<div class="alert alert-danger alert-dismissible">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                            '<strong>Nama ini sudah terdaftar!</strong> Mohon masukkan nama resep yang unik' +
                            '</div>'
                        );
                    } else {
                        $('#id_kategori').prop('disabled', false);
                        $('#id_durasi').prop('disabled', false);
                        $('#id_porsi').prop('disabled', false);
                        $('#id_deskripsi').prop('disabled', false);
                        $('#id_bahan').prop('disabled', false);
                        $('#id_langkah').prop('disabled', false);
                        $('#id_foto').prop('disabled', false);

                        $('#submit_button').prop('disabled', false);
                        $("#submit_button").hover(function() {
                            $(this).css("background-color", "#8c63c2");
                            }, function() {
                            $(this).css("background-color", "#a16ae8");
                        });
                        result.append(
                            '<div class="alert alert-success alert-dismissible">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                            '<strong>Nama resep unik!</strong> Silahkan melanjutkan mengisi :)' +
                            '</div>'
                        );   
                    }
                }
            }
        });
    });
});