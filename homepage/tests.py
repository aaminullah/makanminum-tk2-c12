from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve
from .apps import HomepageConfig
from django.contrib.auth.models import User

# Create your tests here.
class TestApp(TestCase):
    def setUp(self):
        self.credentials = {
            'username' : 'dummyuser',
            'password' : 'fortestingpurpose'
        }
        User.objects.create_user(**self.credentials)
        self.client.login(username='dummyuser', password='fortestingpurpose')

    def test_is_app_available(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_is_profile_page_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/home.html')
    
    def test_homepage_while_user_logged_in(self):
        response = self.client.post('/login/', self.credentials, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/home.html')