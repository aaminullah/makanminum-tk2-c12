from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from .forms import SignUpForm

# Create your views here.
def daftar(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"Berhasil membuat akun dengan username: {username}.")
        return redirect('/login')
    else:
        form = SignUpForm()
    return render(request, "daftar.html", {"form": form})