from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth import get_user_model
from .views import daftar

class LoginandSignupTest(TestCase):
    def setUp(self) -> None:
        self.first_name = 'dummy'
        self.last_name = 'user'
        self.email = 'dummyuser@gmail.com'
        self.username = 'dummyuser'
        self.password = 'fortestingpurpose'
    
    def test_is_daftar_page_exists(self):
        response = self.client.get('/daftar/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'daftar.html')
        found = resolve('/daftar/')
        self.assertEqual(found.func, daftar)

    def test_signup_form(self):
        response = self.client.post('/daftar/', data={
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'username': self.username,
            'password1': self.password,
            'password2': self.password
        })
        self.assertEqual(response.status_code, 302)
        users = get_user_model().objects.all()
        self.assertEqual(users.count(), 1)

    def test_dummy_login(self):
        response = self.client.post('/login/', data={
            'username': self.username,
            'password': self.password
        }, follow=True)
        self.assertFalse(response.context['user'].is_authenticated)