from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from tambahupdate.models import Resep
from .models import Kategori
from django.core import serializers
from django.contrib.auth.decorators import login_required
import urllib.request, json


# Create your views here.
def kategori(request):
    context = {
        'list_resep' : Resep.objects.all().order_by("-id") [:6],
        'list_kategori' : Kategori.objects.all(),
    }
    return render(request, "kategori.html", context)

def spesifik(request, nama):
    context = {
        'kategori' : nama,
        'list_resep_by_kategori' : Resep.objects.filter(kategori = Kategori.objects.get(nama = nama)).order_by("-id")
    }
    return render(request, "kategori_spesifik.html", context)

@login_required(login_url='/login/')
def spesifik_api_view(request, nama):
    all_recipe = Resep.objects.filter(kategori = Kategori.objects.get(nama = nama))
    hasil = []
    for i in all_recipe:
        temp = dict()
        temp['user'] = i.user.username
        temp['nama'] = i.nama
        temp['kategori'] = i.kategori.nama
        temp['durasi'] = i.durasi
        temp['porsi'] = i.porsi
        temp['deskripsi'] = i.deskripsi
        temp['bahan'] = i.bahan
        temp['langkah'] = i.langkah
        hasil.append(temp)
    context = {
        'hasil' : hasil,
    }
    return JsonResponse(context)
