from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve
from .apps import KategoriConfig
from tambahupdate.models import Resep
from .models import Kategori
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from . import models
from .views import spesifik_api_view, spesifik, kategori


# Create your tests here.
class Test_kategori(TestCase):

    def setUp(self):
        user = User.objects.create_user(
            'dummyuser',
            'fortestingpurpose',
            'fortestingpurpose'
        )
        user.save()
        self.client.login(username='dummyuser', password='fortestingpurpose')

    def test_is_app_exist(self):
        self.assertEqual(KategoriConfig.name, 'kategori')
        self.assertEqual(apps.get_app_config('kategori').name, 'kategori')

    def test_is_kategori_page_exist(self):
        response = Client().get('/kategori/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kategori.html")

    def test_is_get_absolute_url_active(self):
        Kategori.objects.create(nama='Daging')
        dummy = Kategori.objects.get(id=1)
        self.assertEqual(dummy.get_absolute_url(), '/kategori/Daging/')

    def test_is_kategori_spesifik_page_exist(self):
        Kategori.objects.create(nama='Daging')
        dummy = Kategori.objects.get(id=1)
        response = Client().get(dummy.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kategori_spesifik.html")

    def test_is_kategori_spesifik_api_works(self):
        dummy = Kategori.objects.create(nama='Daging')
        Resep.objects.create(
            user = get_user_model().objects.get(username='dummyuser'),
            nama = "Rendang",
            kategori = dummy,
            durasi = "2 jam",
            porsi = "10 porsi",
            deskripsi = "Maknyos",
            bahan = "- Daging sapi\n- Bumbu rendang",
            langkah = "- Masak Daging sapinya sampai matang\n- Sajikan"
        )
        dummy = Kategori.objects.get(id=1)
        respon = self.client.get('/kategori/Daging/data-json/')
        self.assertEqual(respon.status_code, 200)
        # found = resolve('/kategori/Daging/data-json')
        # self.assertEqual(found.func, spesifik_api_view)
