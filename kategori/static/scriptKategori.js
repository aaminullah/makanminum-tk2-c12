$("#search").keyup(function() {
    var search_query = $(this).val();
    var kategori_dicari = $("#kategori").text();
    console.log(search_query);
    $.ajax({
        type: "GET",
        url: "/kategori/" + kategori_dicari + "/data-json/",
        success: function(response) {
            $("#searchresults").empty();
            if (response.result !== undefined) {
                var init_table = "";
                init_table += '<table class="table">';
                $("#searchresults").append(init_table);
                var counter = 0;
                for (var i = 0; i < response.result.length; i++) {
                    if (response.result[i].isi.includes(search_query)) {
                        var html = "";
                        html += '<tr>';
                        html += '<td>' + response.result[i].nama + '</td>';
                        html += '</tr>';
                        $("#searchresults").append(html);
                        counter++;
                    }
                }
                $("#searchresults").append('</table>');
                console.log(counter);
            }
        }
    });
});