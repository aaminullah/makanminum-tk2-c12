from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Kritik(models.Model):
    pengirim = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    isi = models.TextField(verbose_name = "Tuliskan kritik dan saran di sini:")