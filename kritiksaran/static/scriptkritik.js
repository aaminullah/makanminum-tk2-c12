$(document).ready(function() {
    $('#submitbutton').prop('disabled', true);
    $('#submitbutton').css("background-color", "#a16ae8");
    $.ajax({
        type: "GET",
        url: "/misc/kritik-dan-saran/data-json/",
        success: function(response) {
            $("#searchresults").empty();
            if (response.result !== undefined) {
                var init_table = "";
                init_table += '<table class="table">';
                init_table += '<tr>';
                init_table += '<th>Testimoni Pengguna</th>';
                init_table += '</tr>';
                $("#searchresults").append(init_table);
                for (var i = 0; i < response.result.length; i++) {
                    var html = "";
                    html += '<tr>';
                    html += '<td>' + response.result[i].isi + '</td>';
                    html += '</tr>';
                    $("#searchresults").append(html);
                }
                $("#searchresults").append('</table>');
            }
        }
    });
});

$("#kritik-content").keyup(function() {
    var teks = $(this).val();
    console.log(teks);
    if (teks != '') {
        $('#submitbutton').prop('disabled', false);
    } else {
        $('#submitbutton').prop('disabled', true);
    }
});

$("#submitbutton").hover(function() {
    $(this).css("background-color", "#8c63c2");
    }, function() {
    $(this).css("background-color", "#a16ae8");
});

$("#kritikform").on("submit", function(event) {
    event.preventDefault();
    console.log($('#kritik-content').val())
    $.ajax({
        type: "POST",
        url: "/misc/kritik-dan-saran/data-json/",
        data: {
            the_isi: $("#kritik-content").val(),
            csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
        },
        success: function(json) {
            $("#kritik-content").val("");
            $('#submitbutton').prop('disabled', true);
            alert("Terima kasih atas kritik dan saran yang diberikan!")
            $.ajax({
                type: "GET",
                url: "/misc/kritik-dan-saran/data-json/",
                success: function(response) {
                    $("#searchresults").empty();
                    if (response.result !== undefined) {
                        var init_table = "";
                        init_table += '<table class="table">';
                        init_table += '<tr>';
                        init_table += '<th>Testimoni Pengguna</th>';
                        init_table += '</tr>';
                        $("#searchresults").append(init_table);
                        for (var i = 0; i < response.result.length; i++) {
                            var html = "";
                            html += '<tr>';
                            html += '<td>' + response.result[i].isi + '</td>';
                            html += '</tr>';
                            $("#searchresults").append(html);
                        }
                        $("#searchresults").append('</table>');
                    }
                },
                error: function() {
                    alert('Mohon maaf, ada kesalahan. Harap coba lagi.')
                }
            });
        }
    });
});

$("#search").keyup(function() {
    var search_query = $(this).val();
    console.log(search_query);
    $.ajax({
        type: "GET",
        url: "/misc/kritik-dan-saran/data-json/",
        success: function(response) {
            $("#searchresults").empty();
            if (response.result !== undefined) {
                var init_table = "";
                init_table += '<table class="table">';
                init_table += '<tr>';
                init_table += '<th>Testimoni Pengguna</th>';
                init_table += '</tr>';
                $("#searchresults").append(init_table);
                var counter = 0;
                for (var i = 0; i < response.result.length; i++) {
                    if (response.result[i].isi.includes(search_query)) {
                        var html = "";
                        html += '<tr>';
                        html += '<td>' + response.result[i].isi + '</td>';
                        html += '</tr>';
                        $("#searchresults").append(html);
                        counter++;
                    }
                }
                $("#searchresults").append('</table>');
                console.log(counter);
            }
        }
    });
});