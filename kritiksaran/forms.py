from django import forms
from .models import Kritik
from django.forms import ModelForm, Textarea

class KritikForm(ModelForm):
    class Meta:
        model = Kritik
        fields = ["isi"]
        widgets = {"isi": Textarea(attrs={'id': 'kritik-content', 'required': True})}