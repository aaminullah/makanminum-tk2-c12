from django.shortcuts import render, redirect
from django.core import serializers
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .models import Kritik
from .forms import KritikForm
import urllib.request, json

# Create your views here.
@login_required(login_url='/login/')
def kritik_submission_view(request):
    my_form = KritikForm(request.POST or None)
    if my_form.is_valid():
        my_form.instance.pengirim = request.user
        my_form.save()
        return HttpResponseRedirect('')
    return render(request, "kritik-submission.html", {"form": my_form})

@login_required(login_url='/login/')
def kritik_api_view(request):
    if request.method == 'POST':
        isikritik = request.POST.get('the_isi')
        submit_kritik = Kritik(pengirim=request.user, isi=isikritik)
        submit_kritik.save()
    all_kritiks = Kritik.objects.all()
    result = []
    for i in all_kritiks:
        temp = dict()
        temp['pengirim'] = i.pengirim.username
        temp['isi'] = i.isi
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)

def about_us_view(request):
    return render(request, "about-us.html")