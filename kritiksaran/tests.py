from django.apps import apps
from django.test import TestCase
from django.urls import resolve
from django.contrib.auth.models import User
from .apps import KritiksaranConfig
from .models import Kritik
from .views import kritik_submission_view, kritik_api_view, about_us_view
from . import models

# Create your tests here.
class TestApp(TestCase):
    def setUp(self): # set a dummy account, then create it and log in
        self.credentials = {
            'username': 'dummyuser',
            'password': 'fortestingpurpose'
            }
        User.objects.create_user(**self.credentials)
        self.client.login(username='dummyuser', password='fortestingpurpose')

    def test_is_app_available(self): # a rhetorical test
        self.assertEqual(KritiksaranConfig.name, 'kritiksaran')
        self.assertEqual(apps.get_app_config('kritiksaran').name, 'kritiksaran')
    
    def test_is_kritik_model_created(self): # is Kritik model created successfully
        kritikan = Kritik.objects.create(isi="Wah websitenya keren! Semangat terus buat developernya!")
        self.assertTrue(isinstance(kritikan, Kritik))
        self.assertEqual(len(Kritik.objects.all()), 1)
    
    def test_is_kritik_submission_page_exists(self): # is Kritik submission page exists
        response = self.client.get('/misc/kritik-dan-saran/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kritik-submission.html")
        found = resolve('/misc/kritik-dan-saran/')
        self.assertEqual(found.func, kritik_submission_view)

    def test_is_form_submission_succeed(self): # is Kritik model could be submitted successfully
        response = self.client.post('/misc/kritik-dan-saran/', data={'isi': "Sangat senang dengan keberadaan MakanMinum."})
        amount = Kritik.objects.all().count()
        self.assertEqual(amount, 1)

    def test_is_kritik_api_works(self): # is Kritik API (as JSON file) works
        response1 = self.client.post('/misc/kritik-dan-saran/', data={'isi': "Sangat senang dengan keberadaan MakanMinum."})
        amount = Kritik.objects.all().count()
        self.assertEqual(amount, 1)
        response2 = self.client.get('/misc/kritik-dan-saran/data-json/')
        self.assertEqual(response2.status_code, 200)
        found = resolve('/misc/kritik-dan-saran/data-json/')
        self.assertEqual(found.func, kritik_api_view)

    def test_is_about_us_page_exists(self): # is About Us page exists
        response = self.client.get('/misc/about-us/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "about-us.html")
        found = resolve('/misc/about-us/')
        self.assertEqual(found.func, about_us_view)