# Generated by Django 3.1.2 on 2020-12-22 07:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kritiksaran', '0003_auto_20201221_0009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kritik',
            name='pengirim',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL),
        ),
    ]
